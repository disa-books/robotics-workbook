# Introduction

## Denavit-Hartenberg algorithm

Denavit y Hartenberg desarrollaron este convencionalismo para describir robots y publicaron un artículo describiéndolo.

El método se basa en la transformación de un sistema de referencia $i$ respecto del anterior $i-1$, empezando desde la base del robot $i=0$. Utiliza matrices homogéneas para describir el cambio de sistema de referencia. Como el nuevo sistema de referencia es variable y depende de $\theta_i$, cada matriz de transformación será una función del ángulo girado, resultando cada matriz:

$
^{i-1}A_i=T(\theta_i)
$

Además de proponer el método, desarrollaron un algoritmo que permite describir cualquier cadena cinemática o robot siguiendo varios pasos. El algoritmo es el siguiente:

![](pictures/dh-1-5.png) ![](pictures/dh-6-11.png) ![](pictures/dh-12-16.png)

Para cada uno de los eslabones se definen cuatro parámetros:

![](pictures/dh-eslabon.png)

Estos parámetros describen por completo la matriz de transformación entre un eslabón y el siguiente $^{i-1}A_i$, siendo el parámetro $\theta_i$ variable $d_i$ si la articulación es prismática, ya que corresponde al grado de libertad de la articulación, lo que permite calcular la orientación y posición del manipulador en función de las coordenadas articulares.

.

