

# Solved problems

## Problem 1

Determine the Euler angles (wuw) associated with the quaternion $q=(\frac{\sqrt{2}}{2},0,0,\frac{\sqrt{2}}{2})$.

### Solution

The intrinsic Euler angles associated with the wuw convention, correspond to three turns on the axes: z,x,z, in that order.

The quaternion can be written as follows:

$
q=\frac{\sqrt{2}}{2}*(1,0,0,1),
$

in such a way that it can be compared term by term with one of the definitions of quaternion, $q = \sin \frac{\theta}{2}*(1/\tan \frac{\theta}{2},u_x,u_y,u_z)$. It can then be stated that:

* $\sin \frac{\theta}{2} = \frac{\sqrt{2}}{2}$.
* $1/\tan \frac{\theta}{2} = 1$.
* $u_x = 0$.
* $u_y = 0$.
* $u_z = 1$.

Or otherwise:

$
\theta = 2*\arcsin{\frac{\sqrt{2}}{2}}
$

$
\theta = 2*\arctan(1)
$

The only angle that satisfies both conditions is $\theta = 1.57\mathrm{[rad]} = 90 \mathrm{[deg]}$.

So the quaternion represents a 90° rotation about the z-axis. The result in radians for this rotation would be:

$
{WUW}=(1.57,0,0)
$

## Exercise 2

Write the quaternion of Euler angles ${WUW} =(0,\pi,0)$.

### Solution

To compose the three rotations, the corresponding quaternions are calculated, and multiplied in the correct order.

In this case, applying the definition of quaternion $Q=(\cos \frac{\theta}{2},sin\frac{\theta}{2} \cdot (e_x,e_y,e_z))$:

* $Q_1=(1,0,0,0)$
* $Q_2=(0,1,0,0)$
* $Q_3=(1,0,0,0)$

In order to chain rotations, they are multiplied in the correct order:

$
(Q_1*Q_2)=(1,0,0,0)*(0,1,0,0) = (0,1,0,0)
$

$
(Q_1*Q_2)*Q3=(0,1,0,0)*(1,0,0,0) = (0,1,0,0)
$

Therefore, the rotation is expressed as follows:

$
(0,\pi,0)_{WUW}=(0,1,0,0)_Q
$

The described rotation is equivalent to a rotation of  $\pi \mathrm{[rad]}$ about the $u$-axis, but since the previous rotation ($w$) is zero, the $u$-axis coincides with the $x$-axis, so the described rotation is $\pi \mathrm{[rad]}$ about the $x$-axis.

Comparing the result with the definition of quaternion, we find that:


* $\cos \frac{\theta}{2}=0 \Rightarrow \frac{\theta}{2}=\frac{\pi}{2},\frac{3\pi}{2}$
* $\sin \frac{\theta}{2}=1 \Rightarrow \frac{\theta}{2}=\frac{\pi}{2} \Rightarrow \theta=\pi$
* $e=(1,0,0) = e_x$

## Exercise 3

Obtain the quaternion associated to:

$$
R=
 \begin{pmatrix}
1 & 0 & 0 \\
0 & 1/\sqrt{2} & -1/\sqrt{2} \\
0 & 1/\sqrt{2} & 1/\sqrt{2} \\
\end{pmatrix}
$$

### Solution

The general case is solved by means of the eigenvalues of the matrix, calculating the axis of rotation (Euler's theorem), and the angle. In this particular case, it can be solved in a simpler way, noticing that the structure of the matrix corresponds to an elementary rotation around the x-axis.

$$
R_x(\alpha) = \begin{pmatrix}
1 & 0 & 0 \\
0 & \cos \alpha &  -\sin \alpha \\
0 & \sin \alpha  &  \cos \alpha \\
\end{pmatrix}=
R=
 \begin{pmatrix}
1 & 0 & 0 \\
0 & 1/\sqrt{2} & -1/\sqrt{2} \\
0 & 1/\sqrt{2} & 1/\sqrt{2} \\
\end{pmatrix}
$$

If the two matrices are equated term by term, the following expressions are obtained:

* $\cos \alpha= 1/\sqrt{2} \quad\Rightarrow \alpha=\pm\pi/4$
* $\sin \alpha= 1/\sqrt{2}\quad\Rightarrow \alpha=\pi/4$

So it is a rotation of 45[deg] about the x-axis: $\theta=\pi/4$, $e=(1,0,0)$. Replacing in the definition of quaternion, it turns out:

$
Q=(\cos\pi/8,\sin\pi/8*(1,0,0))=(0'92,0'38,0,0)
$

## Exercise 6

$O_{xyz}$ transforms 30[deg] about Z, followed by a unit displacement along the new x-axis (u). Find the equations relating the coordinates of a point in both coordinate systems.

### Solution

The representation of this transformation is shown in the figure below.

![](pictures/ejercicio6.svg)

The required result is the transformation of the coordinate origin $^{xyz}T_{uvw}$ in such a way that:

$
^{xyz}p=^{xyz}T_{uvw} \cdot ^{uvw}p
$

Intrinsic transformations can be chained, so that the transformation T can be written as:
$
^{xyz}T_{uvw}=^{xyz}R_{aux} \cdot ^{aux}D_{uvw}
$

Being $aux$ an intermediate coordinate system in which the new axis has already been rotated, but not yet displaced.
Thus, we can decompose the transformation into two simpler transformations, rotation only and translation only.

On the other hand, using homogeneous matrices, $R$ and $D$ can be written as follows:

$$
^{xyz}R_{uvw}=
 \begin{pmatrix}
\cos \gamma & -\sin \gamma & 0 & 0 \\
\sin \gamma & \cos \gamma & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{pmatrix}
$$

$$
^{uvw}D_{uvw}=
\left( \begin{matrix}
1 & 0 & 0 & l \\
0 & 1 & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{matrix} \right)
$$

Substituting the values of $l=1$, $\gamma=30\deg$ and multiplying results:

$$
^{xyz}T_{uvw}=
\left( \begin{matrix}
0,87& -0,50& 0& 0& \\
0,50& 0,87& 0& 0& \\
0& 0& 1& 0& \\
0& 0& 0& 1&
\end{matrix} \right)
\times
\left( \begin{matrix}
1 & 0 & 0 & 1,00 \\
0 & 1 & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{matrix} \right)
=
\left( \begin{matrix}
0,87&    -0,50&    0&    0,87& \\
0,50&    0,87&    0&    0,50& \\
0&    0&    1&    0& \\
0&    0&    0&    1& \\
\end{matrix} \right)
$$

## Ejercicio 8

A camera system mounted on a robot detects the centroid of an object. The detected position is $(\frac{1}{2},\frac{1}{2},1)$ with respect to the camera, but to manipulate the object, its position with respect to the robot needs to be known. Calculate the position of the object relative to the base of the robot if the camera is known to be at position $(1,0,0)$ relative to the base, and points 30º down. All distances are in meters. The coordinate origins of the camera and the robot are shown in the figure.

![](pictures/p8.svg)

### Solución

The first thing is to describe the complete transformation with simple transformations.

First, $^{S1}T_{S2}$ will be calculated, that is, the transformation from the $S_1$ system to the $S_2$ system without the camera base rotation, which will then be applied as $^{S2}T_{S2'}$.

For the calculation, $^{S1}T_{S2}$ is decomposed into the following steps: Rotation: 90[deg] in U (the X-axis) 2. Rotation: 90[deg] in W (the new Y-axis) 3. Translation: (0,1,0).

It should be noted that the translation would be different if it had another position in the list. In general, the resulting transformation always depends on the order of the elementary transformations.

The following figure shows the steps graphically.

![](pictures/p8_1.svg)

In the general case, a homogeneous matrix can be calculated for each step, but several steps can also be combined to form a homogeneous matrix by means of submatrices, as described below.

$$
R=R_x(\pi/2)\times R_y(\pi/2) =
\begin{pmatrix}
1 & 0 & 0 \\
0 & 0 & -1 \\
0 & 1 & 0\\
\end{pmatrix}
\times
\begin{pmatrix}
0 & 0 & 1 \\
0 & 1 & 0 \\
-1 & 0 & 0 \\
\end{pmatrix}
=\begin{pmatrix}
0 & 0 & 1\\
1 & 0 & 0\\
0 & 1 & 0\\
\end{pmatrix}
$$

So steps 1 and 2 are described by the homogeneous matrix:

$$
^{S1}R_{2}=
 \begin{pmatrix}
0 & 0 & 1 & 0 \\
1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 \\
0 & 0 & 0 & 1
\end{pmatrix}
$$

And the translation, step 3, is described by:

$$
^{2}L_{S2}=
\begin{pmatrix}
1 & 0 & 0 & 0 \\
0 & 1 & 0 & 1 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{pmatrix}
$$

To calculate the complete transformation, multiply in the correct order

$
^{S1}T_{S2}=^{S1}R_{2}\times^{2}L_{S2}
$

Y el resultado es:

$$
\begin{pmatrix}
0 & 0 & 1 & 0 \\
1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 \\
0 & 0 & 0 & 1
\end{pmatrix}
\times
\begin{pmatrix}
1 & 0 & 0 & 0 \\
0 & 1 & 0 & 1 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{pmatrix}
=
\begin{pmatrix}
0 & 0 & 1 & 0\\
1 & 0 & 0 & 0\\
0 & 1 & 0 & 1\\
0 & 0 & 0 & 1\\
\end{pmatrix}
=^{S1}T_{S2}
$$

As a curiosity, note that the resulting matrix is equivalent to a translation followed by two rotations, in the following way: Translation: (0,0,1) 2. Rotation: 90[deg] in U (the X axis) 3. Rotation: 90[deg] in W (the new Y axis)

This means that the whole matrix could have been piecewise formed and the last multiplication would have been avoided.

To consider the 30[deg] rotation about the X-axis of the camera system, the homogeneous matrix $^{S2}T_{S2'}$, or what means, the transformation from $S2$ to $S2'$, is used.
Since this is an elementary rotation around the X-axis of $S2$, the rotation matrix is as follows:

$$
R_x(\alpha) = \begin{pmatrix}
1 & 0 & 0 \\
0 & \cos \alpha &  -\sin \alpha \\
0 & \sin \alpha  &  \cos \alpha \\
\end{pmatrix}
=
\begin{pmatrix}
1 & 0 & 0 \\
0 & \sqrt{3}/2 &  -1/2 \\
0 & 1/2  & \sqrt{3}/2 \\
\end{pmatrix}
$$

And the transformation matrix:
$$
^{S2}T_{S2'}=\begin{pmatrix}
1 & 0 & 0 & 0\\
0 & \sqrt{3}/2 &  -1/2 & 0\\
0 & 1/2  & \sqrt{3}/2  & 0\\
0 & 0 & 0 & 1\\
\end{pmatrix}
$$

Combining the two transformations, the result is:

$$
^{S1}T_{S2'}=^{S1}T_{S2} \times ^{S2}T_{S2'}=
\begin{pmatrix}
0 & 0 & 1 & 0\\
1 & 0 & 0 & 0\\
0 & 1 & 0 & 1\\
0 & 0 & 0 & 1\\
\end{pmatrix} 
\times
\begin{pmatrix}
1 & 0 & 0 & 0\\
0 & \sqrt{3}/2 &  -1/2 & 0\\
0 & 1/2  & \sqrt{3}/2  & 0\\
0 & 0 & 0 & 1\\
\end{pmatrix}
$$

$$
^{S1}T_{S2'}=
\begin{pmatrix}
0 & 1/2 & \sqrt{3}/2 & 0\\
1 & 0 & 0 & 0\\
0 & \sqrt{3}/2 & -1/2 & 1\\
0 & 0 & 0 & 1\\
\end{pmatrix}
$$

And finally, to calculate the position of the centroid with respect to the base, we only have to apply the transformation to point $^{S2'}p=(\frac{1}{2},\frac{1}{2},1)$:

$
^{S1}p= ^{S1}T_{S2'} \times ^{S2'}p
$

Replacing the values results in:

$$
^{S1}p=\begin{pmatrix}
0 & 1/2 & \sqrt{3}/2 & 0\\
1 & 0 & 0 & 0\\
0 & \sqrt{3}/2 & -1/2 & 1\\
0 & 0 & 0 & 1\\
\end{pmatrix}
\times
\begin{pmatrix}
\frac{1}{2}\\
\frac{1}{2}\\
1\\
1\\
\end{pmatrix}
=
(1'12,0'50,0'93,1)
$$

## Exercise 9

A robot with 6 degrees of freedom must pick up a part lying on a table. The position and orientation of the part is known from the table coordinate system, but to program the robot, the position and orientation of the part with respect to the robot base is needed. The reference systems are detailed in the figure. Calculate the target needed to program the robot in both position and orientation.
The distances in the figure are in millimeters.

Vista superior: ![](pictures/p9a.svg) Vista lateral: ![](pictures/p9b.svg)

### Solution

The first thing is to describe the complete transformation with simple transformations.

The requested data can be calculated by tranforming from the $S0$ coordinate system at the base of the robot to the target-anchored $S2$ coordinate system shown in the top view.
Specifically what is sought is the $^{S0}T_{S2}$ transformation, which can be decomposed into:

$
^{S0}T_{S2}=^{S0}T_{S1}*^{S1}T_{S2}
$

For the calculation, $^{S0}T_{S1}$ is decomposed into the following steps: Translation: (1,1,0.825) 2. Rotation: -90[ðeg] in Z

$^{S1}T_{S2}$ decomposes into the following steps: 1. translation: (1,1,0) 2. rotation: 30[deg] in Z

For $^{S0}T_{S1}$ steps 1 and 2 are described by the homogeneous matrix formed by the translation vector and an elementary rotation matrix in Z:

$$
^{S0}T_{S1}=
 \begin{pmatrix}
\cos \theta_1 & -\sin \theta_1 & 0 & t_{x1} \\
\sin \theta_1 & \cos \theta_1 & 0 & t_{y1} \\
0 & 0 & 1 & t_{z1} \\
0 & 0 & 0 & 1
\end{pmatrix}=
 \begin{pmatrix}
 0 & 1 & 0 & 1\\
-1 & 0 & 0 & 1\\
0 & 0 & 1 & 0,825\\
0 & 0 & 0 & 1\\
\end{pmatrix}
$$

The two steps are integrated into a single matrix, since the translation is applied **before** the rotation.

For $^{S1}T_{S2}$ the homogeneous matrix is as follows:

$$
^{S1}T_{S2}=
 \begin{pmatrix}
\cos \theta_2 & -\sin \theta_2 & 0 & t_{x2} \\
\sin \theta_2 & \cos \theta_2 & 0 & t_{y2} \\
0 & 0 & 1 & t_{z2} \\
0 & 0 & 0 & 1
\end{pmatrix}=
 \begin{pmatrix}
0,87 & -0,5 & 0 & 0,5\\
0,5 & 0,87 & 0 & 0,5\\
0 & 0 & 1 & 0\\
0 & 0 & 0 & 1\\
\end{pmatrix}
$$

To calculate the complete transformation, the following are multiplied in the correct order

$
^{S0}T_{S2}=^{S0}T_{S1}*^{S1}T_{S2}
$

Which results in:

$$
\begin{pmatrix}
 0 & 1 & 0 & 1\\
-1 & 0 & 0 & 1\\
0 & 0 & 1 & 0,825\\
0 & 0 & 0 & 1\\
\end{pmatrix}
\times
 \begin{pmatrix}
0,87 & -0,5 & 0 & 0,5\\
0,5 & 0,87 & 0 & 0,5\\
0 & 0 & 1 & 0\\
0 & 0 & 0 & 1\\
\end{pmatrix}
=
\begin{pmatrix}
0,50 & 0,87 & 0 & 1,5\\
-0,87 & 0,50 & 0 & 0,5\\
0 & 0 & 1 & 0,825\\
0 & 0 & 0 & 1\\
\end{pmatrix} 
=^{S0}T_{S2}
$$

Comparing the result with the definition of homogeneous matrix and that of elementary rotation in Z, it can be seen that it is a translation $(1'5,0'5,0'825)$ , followed by a rotation of  $\theta=-60 \deg$.

In Rapid language, this data corresponds to a target type, and would be defined as follows:

```text
CONST robtarget objetivo := [[1.5,0.5,0.825],[0.866,0,0,-0.5],[0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
```

It should be remembered that the quaternion associated with the rotation of -60 degrees around the Z axis corresponds to:

$
Q=(\cos \frac{\theta}{2},sin\frac{\theta}{2} \cdot (e_x,e_y,e_z))=
(0'866,-0'5*(0,0,1))=
(0'866,0,0,-0'5)
$

## Proposed Exercises

## Proposed 1

A trajectory defined by two coordinate systems $S_1$ and $S_2$ is available. These systems define the position and orientation of the robot manipulator with respect to the base $(S_0)$. Find an intermediate coordinate system (halfway in distance and angle) with respect to the base. The data are as follows (position in meters, rotation in quaternions):

| System | Position | Rotation |
| :--- | :--- | :--- |
| $^0S_1$ | $(1,1,1)$ | $(1,0,0,0)$ |
| $^0S_2$ | $(1,-1,1)$ | $(0,0,1,0)$ |

## Solutions to the proposed exercises

Proposed 1: $^0S_m$ : Pos: $(1,0,1)$ Rot: $(0.7071,0,0.7071,0)$


