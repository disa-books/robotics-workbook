

# Concepts


## Industrial robot

An industrial robot is any robot designed to meet the needs of an industrial process.

The industrial robots are composed of two basic parts, links and joints. A link is a solid with two unique ends. Each of these ends is part of a joint, except the first link that is anchored to the floor.

![](pictures/drawing1.svg)

Although there are many types of robots other than industrial, most are also composed of joints and links (kinematic chain), so you can apply the same concepts and reasoning to non-industrial robots.

## Resolver

The operation principle of a resolver is shown in the following figure:

![](pictures/resolver.png)

The voltage Vr is a sinusoidal electric current that flows through the rotor coil. This current produces a sinusoidal change in the magnetic field which is detected by two coils located in the stator.

* $Vs = V\cdot\sin(\omega t)\cdot\sin(\theta_m)$,
* $Vc = V\cdot\sin(\omega t)\cdot\cos(\theta _m)$,

One of the stator coils is made to coincide with the original position of the rotor (0 deg), and is called a cosine coil, because the current induced by the magnetic field of the rotor coincides with the cosine of the rotor angle.
The other coil is phased out (90 deg) to detect the quadrant in which the rotor is located by the sign of the voltage compared to Vr (phase).
The relationship between the voltage induced in the stator and the reference voltage for the described resolver is as follows:


* ++ : q1
* +- : q2
* -- : q3
* -+ : q4

This distribution can be seen in the following figure:

 ![](pictures/resolver_cuadrantes.svg)

## Quadrature incremental encoder

It is a type of incremental encoder that uses two optical sensors with a 1/4 slot displacement from each other.

This arrangement generates two 90 deg offset or quadrature pulse signals. These are usually called A and B signals. Thanks to these two signals, it is possible to know the direction of rotation of the axis. If signal A overtakes signal B, the axis rotates in one direction, while if B overtakes A, the direction will be the opposite.

In addition, a quadrature encoder, allows detecting four pulses for each slot of the encoder, since the signals A and B will change four times during the passage through the slot:

    1. (A) Goes from 0 to 1.
    2. (B) Goes from 0 to 1.
    3. (A) Goes from 1 to 0.
    4. (B) Goes from 1 to 0.

The following figure shows the value of the signals A and B during the passage through a slot of the encoder.

![](pictures/quadratureterms.jpg)


