# Introduction

This chapter will describe the industrial robots, as well as the fundamental parts of which they are composed.

Useful concepts and mathematical tools applied to the study and design of the robots in general are also covered.
