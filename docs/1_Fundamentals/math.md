

# Math tools

## Euler angles

The three Euler angles is a convention used to describe the spatial rotation of a solid object.
According to this convention, there are two types of rotations; intrinsic and extrinsic.

* Extrinsic: it is performed around an axis external to the object.
* Intrinsic: it is performed around an axis linked to the object.

To describe the Euler rotation, the letters are used:

* X,Y,Z for initial axes.
* U,V,W to denote the new axes.
* $\alpha, \phi, \theta$ for the angles.

The following animation shows a rotation using three intrinsic Euler angles. The extrinsic axis (blue) remains unchanged, while the intrinsic axes (red, yellow and green) vary with respect to the extrinsic axis.

![](pictures/euler2a.gif)

If the axis pointing upwards is the Z-axis, and the axis pointing to the left is the X-axis, the rotation shown corresponds to the WUW conventionalism (also called ZXZ in the literature), that is:

* W: Rotation in Z (intrinsic).
* U: Rotation in X (intrinsic).
* W: Rotation in Z (intrinsic).

## Quaternions

A quaternion is a vector of four values represented in the basis $(1,i,j,k)$. The basis $1$ represents the real line, and therefore the first number of the vector corresponds to a scalar. The bases $(i,j,k)$ represent three perpendicular imaginary axes, and can be seen as a representation of three-dimensional space.

One of the applications of quaternions is the description of rotations in space.

According to Euler's rotation theorem, for any rotation or combination of rotations of a rigid solid in three-dimensional space, there exists an axis of rotation and an angle describing the rotation. Quaternions can be used to represent this axis-angle as follows:

$
\mathbf{q} = e^{\frac{\theta}{2}{(u_x\mathbf{i} + u_y\mathbf{j} + u_z\mathbf{k})}} =
\cos \frac{\theta}{2} + (u_x\mathbf{i} + u_y\mathbf{j} + u_z\mathbf{k}) \sin \frac{\theta}{2} =
\sin \frac{\theta}{2} (1/\tan \frac{\theta}{2} + (u_x\mathbf{i} + u_y\mathbf{j} + u_z\mathbf{k}))
$

Which can also be written with basis vector notation $(1,i,j,k)$ :

$
\mathbf{q} = (\cos \frac{\theta}{2} ,\sin \frac{\theta}{2} \cdot u_x ,\sin \frac{\theta}{2} \cdot u_y,\sin \frac{\theta}{2} \cdot u_z)=
\sin \frac{\theta}{2}*(1/\tan \frac{\theta}{2} , u_x , u_y, u_z)
$

Where $\theta$ is the angle of rotation around the axis, and $(u_x\mathbf{i} + u_y\mathbf{j} + u_z\mathbf{k})$ is the unit vector defining the axis of rotation.

One of the advantages of quaternions is that to concatenate **intrinsic** rotations, one need only multiply the quaternions in the correct order.
It should be noted that this is not the common vector product. The multiplication of two quaternions is performed by an operation called the Hamiltonian product, in which the basis vectors are also multiplied.

The multiplication of quaternions $q_a=(a_1+a_2i+a_3j+a_4k) ; q_b=(b_1+b_2i+b_3j+b_4k)$ is done as follows:


\begin{matrix}
q_a * q_b =\\
a_1b_1+a_1b_2i+a_1b_3j+a_1b_4k+\\
a_2ib_1+a_2ib_2i+a_2ib_3j+a_2ib_4k+\\
a_3jb_1+a_3jb_2i+a_3jb_3j+a_3jb_4k+\\
a_4kb_1+a_4kb_2i+a_4kb_3j+a_4kb_4k\\
\end{matrix}


Rearranging terms, and taking into account that:

$
i^2=j^2=k^2=ijk=-1
$

\begin{matrix}
ij  = k\\
ji  = -k\\
jk  = i\\
kj  = -i\\
ki  = j \\
ik  = -j
\end{matrix}

The result is as follows:

$
q_a * q_b = (a_1b_1 - a_2b_2 - a_3b_3 - a_4b_4)
+ (a_1b_2 + a_2b_1 + a_3b_4 - a_4b_3)i
+ (a_1b_3 - a_2b_4 + a_3b_1 + a_4b_2)j +
(a_1b_4 + a_2b_3 - a_3b_2 + a_4b_1)k
$

## Rotation matrix

A rotation matrix describes a rotation in Euclidean space.

If the following figure is considered:

![](pictures/rotmat_active.svg)

The point defined by the vector $p=(x,y)$ performs a rotation around the origin of coordinates.
The new rotated point, $p'$, is equivalent to:

$$
\begin{pmatrix}
x' \\
y' \\
\end{pmatrix} =
\begin{pmatrix}
\cos \theta & -\sin \theta \\
\sin \theta & \cos \theta \\
\end{pmatrix}
\begin{pmatrix}
x \\
y \\
\end{pmatrix}
$$

$
p'=
R\cdot p
$

Where $R$ is the rotation matrix.
This type of rotation is called active transformation, and is useful for calculating rotations **without** changing the origin of coordinates.

If the intention is to describe a change of coordinates, as in the following figure:

![](pictures/rotmat_passive.svg)

The rotation matrix is used to obtain the coordinates of the vector $p$ in the other reference frame by the following equation:

$$
\begin{pmatrix}
x \\
y \\
\end{pmatrix} =
\begin{pmatrix}
\cos \theta & -\sin \theta \\
\sin \theta & \cos \theta \\
\end{pmatrix}\begin{pmatrix}
u \\
v \\
\end{pmatrix}
$$

Or in a similar way:

$
p_{xy}=R \cdot p_{uv}
$

Note that the above equation calculates the coordinates of the point $p$ in the XY system from the coordinates of $p$ in the UV system. If what is sought are the coordinates of $p$ in the UV basis, the correct equation would be:

$
p_{uv}=R^{-1} \cdot p_{xy}
$

As a reminder of how the matrix works, it is denoted as follows:

$
p_{xy}=^{xy}R_{uv} \cdot p_{uv}
$

Similarly, the change of coordinate system in three dimensions can be described by a rotation matrix.

$
p_{xyz}= ^{xyz}R_{uvw} \cdot p_{uvw}
$

Since in three-dimensional space, the orientation of a solid is described by three elementary rotations by Euler's theorem, the matrix $R$ can be decomposed into three elementary rotations. In order to compose the rotations, they must be intrinsic, so the above equation can be written as:

$
p_{xyz}=R_u \times R_v \times R_w \cdot p_{uvw}
$

Do not forget that matrix multiplication **is not** commutative, so the order of the rotations is important.

## Basic rotations

A rotation is considered elemental when it is limited to an angle around one of the coordinate axes of a reference system. For the XYZ coordinate axes, the basic rotation matrices are as follows:

$$
R_x(\alpha) = \begin{pmatrix}
1 & 0 & 0 \\
0 & \cos \alpha &  -\sin \alpha \\
0 & \sin \alpha  &  \cos \alpha \\
\end{pmatrix}
$$

$$
R_y(\phi) = \begin{pmatrix}
\cos \phi & 0 & \sin \phi \\
0 & 1 & 0 \\
-\sin \phi & 0 & \cos \phi \\
\end{pmatrix}
$$

$$
R_z(\theta) = \begin{pmatrix}
\cos \theta &  -\sin \theta & 0 \\
\sin \theta & \cos \theta & 0\\
0 & 0 & 1\\
\end{pmatrix}
$$

## Homogeneous transformation matrices

A transformation between two coordinate systems can be represented by homogeneous matrices.
A homogeneous vector or matrix has one dimension more than the space it represents, therefore a homogeneous matrix will have dimension 4x4 for three dimensional space.

This matrix is composed of four submatrices:

* A rotation matrix of 3x3
* A 1x3 translation vector
* A 3x1 perspective vector
* A scaling scalar (1x1)

Its location within the homogeneous matrix is as follows:

$$
\begin{pmatrix}
Rotation & Translation\\
Perspective & Scaling \\
\end{pmatrix}
$$

$$
\begin{pmatrix}
\begin{bmatrix}
r_{11} & r_{12} & r_{13} \\
r_{21} & r_{22} &  r_{23} \\
r_{31} & r_{32}  &  r_{33} \\
\end{bmatrix} & \begin{bmatrix}
t_x \\
t_y \\
t_z \\
\end{bmatrix} \\
\begin{bmatrix}
p_1 & p_2 & p_3
\end{bmatrix} & [w_1] \\
\end{pmatrix}
$$

In robotics, perspective and scaling are not necessary, so the transformation matrix reduces to:

$$
\begin{pmatrix}
\begin{bmatrix}
r_{11} & r_{12} & r_{13} \\
r_{21} & r_{22} &  r_{23} \\
r_{31} & r_{32}  &  r_{33} \\
\end{bmatrix} & \begin{bmatrix}
t_x \\
t_y \\
t_z \\
\end{bmatrix} \\
\begin{bmatrix}
0 & 0 & 0
\end{bmatrix} & \begin{bmatrix}
1
\end{bmatrix} \\
\end{pmatrix}
$$

And it is usually written without the brackets:

$$
\begin{pmatrix}
r_{11} & r_{12} & r_{13} & t_x\\
r_{21} & r_{22} &  r_{23} & t_y \\
r_{31} & r_{32}  &  r_{33} & t_z\\
0 & 0 & 0 & 1\\
\end{pmatrix}
$$

## Inverse of a homogeneous transformation matrix

Homogeneous matrices are particularly used in the robot kinematics calculation. For direct kinematic calculation no additional operation has to be done, however for inverse kinematic calculation it is necessary to find the inverses of the homogeneous matrix in some cases. Homogeneous matrices can be inverted in parts, since it is a matrix formed by submatrices. Applying the properties of rotation matrices can make the operations much simpler. To find the inverse matrix of the transformation $^{S0}A_{S1}$, the considerations to take into account are: 1. The orientation of S0 with respect to S1 corresponds to the inverse matrix of rotation. 2. The inverse of the rotation matrix corresponds to its transpose: $R^{-1}=R^t$. 3. The inverse translation corresponds to the translation up to S0 with respect to S1. 4. The translation with respect to S1 is the same as the rotated translation and of opposite sign: $-R^{-1}*t$.

Therefore, for any homogeneous transformation matrix$^{S0}A_{S1}$, the inverse matrix can be constructed piecewise as follows:

$$
^{S0}A_{S1}
=
\begin{pmatrix}
& R& & t\\
0 & 0 & 0 & 1\\
\end{pmatrix}
$$

$$
(^{S0}A_{S1})^{-1}
=
\begin{pmatrix}
& R^{-1}& & -R^{-1}*t\\
0 & 0 & 0 & 1\\
\end{pmatrix}
$$


## Linear composition of sinusoidal functions

Any linear combination of sine functions of the same frequency is a sine wave of the same frequency but with different phase and amplitude.

In equation form it would be:

$
a \sin {(\alpha)} + b \cos {(\alpha)} = \sqrt{a^2+b^2} \sin {(\alpha+\arctan{\frac{b}{a}})}
$

The proof of this trigonometric identity can be done by means of the identity of the sine of the sum of angles. It is enough to substitute $a=l\cdot \cos {(\gamma)}$ and $b=l\cdot \sin {(\gamma)}$. Once the substitution is done, we are left with:

$
a \sin {(\alpha)} + b \cos {(\alpha)} = l \cos {(\gamma)}\sin {(\alpha)} + l \sin {(\gamma)}\cos {(\alpha)}=
l \sin {(\gamma+\alpha)}
$

To compute $l$ and $\gamma$ the terms $a^2+b^2$ y $b/a$ are introduced in the substitution equations ($a=l\cdot \cos {(\gamma)}$ and $b=l\cdot \sin {(\gamma)}$), resulting:

* $l=\sqrt{a^2+b^2}$
* $\gamma=\arctan{(b/a)}$

The results in phase shift depend on the signs of a and b. Specifically, when a and b correspond to the identity of subtraction, the phase difference will be backward (negative).

There is another equation for linear composition that uses the cosine identity of the sum of angles.



