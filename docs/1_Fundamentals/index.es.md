# Introducción

En este capítulo se describirán los robots industriales, así como las partes fundamentales de las que se componen.

También se describen conceptos útiles y herramientas matemáticas aplicadas al estudio y diseño de los robots en general.

