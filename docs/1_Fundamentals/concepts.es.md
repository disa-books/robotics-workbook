

# Conceptos


## Robot industrial

Un robot industrial es cualquier robot diseñado para satisfacer las necesidades de un proceso industrial.

Los robots industriales se componen de dos partes básicas, eslabones y articulaciones. Un eslabón es un sólido con dos extremos únicos. Cada uno de estos extremos forma parte de una articulación, excepto el primer eslabón que está anclado al suelo.

![](pictures/drawing1.svg)

Aunque existen muchos tipos de robots distintos de los industriales, la mayoría también están compuestos por articulaciones y eslabones (cadena cinemática), por lo que puedes aplicar los mismos conceptos y razonamientos a los robots no industriales.

## Resolver

El principio de funcionamiento de un resolver se muestra en la siguiente figura:

![](pictures/resolver.png)

La tensión Vr es una corriente eléctrica sinusoidal que circula por la bobina del rotor. Esta corriente produce un cambio sinusoidal en el campo magnético que es detectado por dos bobinas situadas en el estator.

* $Vs = V\cdot\sin(\omega t)\cdot\sin(\theta_m)$,
* $Vc = V\cdot\sin(\omega t)\cdot\cos(\theta _m)$,

Una de las bobinas del estator se hace coincidir con la posición original del rotor (0 grados), y se denomina bobina de coseno, porque la corriente inducida por el campo magnético del rotor coincide con el coseno del ángulo del rotor.
La otra bobina está desfasada (90 grados) para detectar el cuadrante en el que se encuentra el rotor por el signo de la tensión respecto a Vr (fase).
La relación entre la tensión inducida en el estátor y la tensión de referencia para el resolver descrito es la siguiente:


* ++ : q1
* +- : q2
* -- : q3
* -+ : q4

Esta distribución puede verse en la siguiente figura:

 ![](pictures/resolver_cuadrantes.svg)

## Encóder incremental en cuadratura

Es un tipo de codificador incremental que utiliza dos sensores ópticos con un desplazamiento de 1/4 de ranura entre sí.

Esta disposición genera dos señales de impulsos desplazadas 90 grados o en cuadratura. Suelen denominarse señales A y B. Gracias a estas dos señales, es posible conocer el sentido de giro del eje. Si la señal A adelanta a la señal B, el eje gira en un sentido, mientras que si B adelanta a A, el sentido será el contrario.

Además, un encoder de cuadratura, permite detectar cuatro impulsos por cada ranura del encoder, ya que las señales A y B cambiarán cuatro veces durante el paso por la ranura:

  1. (A) Pasa de 0 a 1.
  2. (B) Pasa de 0 a 1.
  3. (A) Pasa de 1 a 0.
  4. (B) Pasa de 1 a 0.

La siguiente figura muestra el valor de las señales A y B durante el paso por una ranura del encoder.

![](pictures/quadratureterms.jpg)



