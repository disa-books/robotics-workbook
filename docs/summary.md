# Table of contents

* [Introduction](index.md)
* [Fundamentals](1_Fundamentals/index.md)
    * [Basic concepts](1_Fundamentals/concepts.md)
    * [Math tools](1_Fundamentals/math.md)
    * [Solved problems](1_Fundamentals/problems.md)
* [Morphology](2_Morphology/index.md)
    * [Theoretical Background](2_Morphology/background.md)
    * [Solved problems](2_Morphology/problems.md)
* [Kinematics](3_Kinematics/index.md)
    * [Denavit–Hartenberg](3_Kinematics/denavithartenberg.md)
    * [Direct Kinematics](3_Kinematics/cinematica_directa.md)
    * [Inverse Kinematics](3_Kinematics/cinematica_inversa.md)
* [Differential Kinematics](4_Differential_Kinematics/index.md)
    * [Direct Differential Kinematics](4_Differential_Kinematics/cinematica_d_directa.md)
    * [Inverse Differential Kinematics](4_Differential_Kinematics/cinematica_d_inversa.md)
* [Trajectories](5_Trajectories/trayectorias.md)


