
# Problemas resueltos

## Problema 1

![](pictures/MorP1.svg)

El mecanismo de la figura consiste en una transmisión del tipo paralelogramo para mover una articulación de un robot.

La distancia entre el eje del motor-husillo y el soporte de la articulación permanece constante a 250&nbsp;mm, siendo extensible el brazo que aparece a la izquierda de la articulación para mantener esa distancia constante.

El paralelogramo es movido por un motor AC con resolver, cuyo par máximo es de 0,3&nbsp;N·m.

Este conjunto motor-husillo ofrece una relación de reducción de 2000&nbsp;m⁻¹ que permite transformar el movimiento de giro del motor en movimiento de traslación del husillo, con un rendimiento del 80&nbsp;%.

Las ecuaciones del resolver son:

 * $V_1 = V\cdot\sin(\omega t)\cdot\sin(\theta_m)$
 * $V_2 = V\cdot\sin(\omega t)\cdot\cos(\theta_m)$

Siendo $\theta_{m}$ el ángulo en el que se encuentra el motor y $V\,sin(\omega t)$ la tensión de excitación de la bobina móvil del resolver con $V=24\,V$. Los valores iniciales del resolver se fijan en $V1=0\,V$, $V2=0\,V$, $\theta_m=0$ y $\theta_{j}=0$.

 1. Calcular el peso máximo $Q$ que puede soportar el mecanismo en su extremo.
 2. Obtener la relación entre el ángulo girado por el motor $\theta_m$ y el ángulo girado por la articulación $\theta_{j}$.
 3. Si el motor avanza en sentido positivo dando 150 vueltas e indicando unos valores finales en el instante de parada de $V1=11.91\,V$ y $V2=10\,V$, ¿cuál es el valor de $\theta_{j}$ en el punto de parada si la posición cero es la que aparece dibujada?
 4. ¿Cual sería el rango de la articulación si la máxima longitud del brazo extensible es de 600&nbsp;mm?

### Solución

#### Apartado 1

La elongación mínima del brazo extensible se corresponde con la posición inicial del brazo, por lo que el caso más restrictivo de cara al cálculo del peso a soportar será el de la configuración que se muestra en la figura siguiente.

![](pictures/MorP1_1.svg)

En el equilibrio, los pares $\tau _{Q}$ y $\tau _{F}$ se igualan ($\tau _{Q} = \tau _{F}$), teniendo que:

 *  $\tau _{Q} = Q \cdot 1.2$
 *  $\tau _{F} = F \cdot 0.25$

Entonces:

$$Q \cdot 1.2 -  F \cdot 0.25 =0 \quad \Longrightarrow \quad F = 4.8 \cdot Q$$

Por otro lado, las fuerzas en el husillo se pueden determinar fácilmente por un balance de energía.

En la situación ideal, la energía transmitida por el motor al husillo debería ser la misma que la recibida por el brazo, es decir, $E_{rot} = E_{lin}$. Sin embargo, existe una pérdida de energía representada por $\eta$, tal que $\eta \cdot E_{rot} = E_{lin}$. Este coeficiente $\eta$ es siempre menor que la unidad, lo que significa que la energía recibida por el husillo es menor que la transmitida por el motor.

Además, estas energías se pueden interpretar en términos de trabajo a realizar para producir un movimiento:

 *  $E_{rot} = W_{rot} = \tau _{M} \cdot \theta_m$, donde $\tau _{M}$ es el par generado por el motor y $\theta$ es la distancia angular recorrida por el eje del motor (en radianes).
 *  $E_{lin} = W_{lin} = F \cdot d$, donde $F$ es la fuerza recibida en el husillo y $d$ es la distancia lineal que ha recorrido.

Operando:

$$
\eta = {W_{lin} \over W_{rot}} = { {F \cdot d} \over {\tau _{M} \cdot \theta_m} } \quad \Longrightarrow \quad F = {\theta_m \over d} \cdot \tau _{M} \cdot \eta
$$

De acuerdo con el enunciado, la relación de transmisión motor-husillo es ${\theta_m \over d} = 2000\,m^{-1}$. Nótese que este valor no es adimensional debido a que se relaciona un movimiento rotacional con uno lineal. Ante la ausencia de unidades en el numerador (rotación), podemos asumir que este valor se interpreta en radianes ($rad / {m^{-1}}$).

Sustituyendo:

$$
F = {\theta_m \over d} \cdot \tau _{M} \cdot \eta = 2000 \cdot 0.3 \cdot 0.8 = 480\,N
$$

Puesto que $F = 4.8 \cdot Q$, entonces $Q = {F \over 4.8} = {480 \over 4.8} = 100\,N \approx 10\,kg$.

#### Apartado 2

La transmisión del husillo tiene la relación $N = {\theta_m \over d} = 2000\,rad/m$. Ahora se calcula la equivalencia entre $\theta_{j}$ y $d$.

![](pictures/MorP1_2.svg)

De acuerdo con la figura, las ecuaciones trigonométricas que relacionan $d$ con $\theta_{j}$ son

 *  $h\cdot\sin{\theta_{j}} = d$
 *  $h\cdot\cos{\theta_{j}} = 0.25$

Dividiendo estas dos ecuaciones, resulta:

$$
\tan{\theta_{j}} = {d \over 0.25}
$$

Sustituyendo con la expresión de la relación de transmisión:

$$
{ \tan{\theta_{j}} = {{\theta_m}\over{N \cdot 0.25}} = {{\theta_m}\over{2000 \cdot 0.25}}} \quad \Longrightarrow \quad {\theta_{j}} = {\arctan {\theta_m \over 500} }
$$

Nótese que, para acomodarse a las unidades de $N$, el ángulo $\theta_m$ debe estar expresado en radianes.

#### Apartado 3

Dividiendo las ecuaciones del resolver tenemos que:

$$
{ {V_1 \over V_2}={ {V\cdot\sin(\omega t)\cdot\sin(\theta_m)}\over{
 V\cdot\sin(\omega t)\cdot\cos(\theta _m)} } }\quad \Longrightarrow \quad
{ {V_1 \over V_2} ={ \tan(\theta _m) } }
$$

Dado que solo los valores instantáneos de $V_1$ y $V_2$ son conocidos en reposo, no es posible saber a priori si están en fase con la onda senoidal de referencia del resolver. Se asume que sí lo están, por lo que los valores de $\sin(\theta_m)$ y $\cos(\theta_m)$ van a ser ambos positivos, lo que implica que el ángulo de rotación debe encontrarse en el primer cuadrante.

Sustituyendo, el valor actual de $\theta_m$ es:

$$
{ {V_1 \over V_2} ={ \tan(\theta _m) } }= {11.91 \over 10} = {1.191}\quad \Longrightarrow \quad
{\theta _m} = {\arctan(1.191)}= {0.87\,rad \approx 50\,deg}
$$

Considerando que el eje del motor ha dado 150 vueltas ($150 \cdot 2 \pi = 942.48\,rad$), en total su ángulo girado es $\theta_m = 943.35\,rad$. Sustituyendo este valor en la ecuación del apartado 2, se obtiene un giro en la articulación de:

$$
{\theta_j} = {\arctan {943.35 \over 500}} \quad \Longrightarrow \quad {\theta_j} = {1.08 \, rad \equiv 62.08 \, deg}
$$

#### Apartado 4

Teniendo en cuenta que $h\cdot\cos({\theta_{j}}) = 0.25$ (ver apartado 2) y $h = 0.6\,m$, resulta:

$$
{\cos(\theta_{j})} = 0.42\quad \Longrightarrow \quad \theta_{j} = 1.14 \, rad \equiv 65.38 \, deg \approx 65 \, deg
$$

Esto significa que el rango de la articulación es de $\pm 65$&nbsp;deg.

## Problema 2

![](pictures/MorP2_1.png)

Dado el robot de 5 GDL de la figura:

a) La articulación 3 ($q_3$) se mueve mediante un conjunto Motor AC + Freno + Harmonic Drive, siendo los datos de este último: relación de reducción 120:1, rendimiento del 93&nbsp;%.

1. Si la carga estática máxima qye ha de transportar el robot es de 80&nbsp;Newton, ¿cuál debe ser el par máximo (en Newton por metro) que debe soportar un freno situado en el eje del motor aplicando un coeficiente de seguridad del 30&nbsp;%?
2. Si el motor gira a 900&nbsp;rpm, ¿a qué velocidad está girando la articulación?

b) La articulación 1 ($q_1$) se mueve mediante un conjunto Motor AC + Freno + Resolver + Harmonic Drive + Transmisión piñón-cremallera. El Harmonic Drive tiene una relación 175:1 y un rendimiento del 95&nbsp;%. En la transmisión piñón-cremallera, el radio efectivo del piñón es de 0.3&nbsp;metros, y su rendimiento del 75&nbsp;%.

Las ecuaciones de las tensiones del resolver son:

 * $V_1 = V\cdot\sin(\omega t)\cdot\sin(\theta_m)$,
 * $V_2 = V\cdot\sin(\omega t)\cdot\cos(\theta_m)$.

siendo $q_m$ el ángulo en que está situado el motor y $V\,sin(\omega t)$ la tensión de excitación de la bobina móvil del resolver con $V=24\,V$.

1. Si se parte de la posición con $V_1 = 0\,V$, $V_2 = 0\,V$ y $q_1 = 0\,m$, y el motor avanza en sentido positivo indicando el resolver 350 pasos por cero, ¿cuál es el valor de $q_1$ en el punto de parada con los valores $V_1 = 17.32\,V$ y $V_2 = 10\,V$?
2. ¿Cuál será la velocidad lineal de la articulación si el motor gira a 1100&nbsp;rpm?

### Solución

#### Apartado a1

Si la carga máxima real es de 80&nbsp;N, aplicando un coeficiente de seguridad del 30&nbsp;% la carga considerada será:

$$Q=1.3\cdot80\,N=104\,N$$

La configuración más restrictiva (la que requiere que el par en la articulación sea máximo) se muestra en la figura siguiente, con $q_3 = 0º$ y $q_4 = 90º$.

![](pictures/MorP2_2.svg)

El par en $q_3$ será $\tau _{3}=P\,(d+e)$, por lo que $\tau _{3} = 104\,(d+e)$.

Dado que el freno se ubica sobre el eje del motor, es necesario emplear la relación de transmisión indicada en el enunciado, considerando unas pérdidas por fricción dadas por $\eta ={ 0.93 }$. Por el balance de energía entre entradas (motor) y salidas (articulación), sabiendo que toman la forma de trabajo mecánico, se tiene:

$$
\eta = {E_{s} \over E_{e}} = {W_{3} \over W_{m3}} ={ {\tau_{3} \cdot \theta_{3}} \over {\tau_{m3} \cdot \theta_{m3}} } \quad \Longrightarrow \quad \tau_{m3} = { {\tau_3 \cdot \theta_{3}} \over {\eta \cdot \theta_{m3}} }
$$

Sustituyendo:

$$
\tau _{m3} = { {{104\,(d+e)} \over 0.93 \cdot 120 }  } = {0.932\,(d+e)} \quad  [\text N\cdot \text m]
$$

#### Apartado a2

Tomando que la relación de transmisión es $N=120$:

$$
N={\omega_{i} \over \omega_{o} } = {\omega_{m3} \over \omega_{3} } \quad \Longrightarrow \quad {\omega_{3}} = {\omega_{m3}  \over N  }
$$

Sustituyendo, resulta:

$$
\omega_{3} = {900 \over 120 } = {7.5}\,\text{rpm}
$$

#### Apartado b1

Dividiendo las ecuaciones del resolver:

$$
{ {V_1 \over V_2} ={ \tan(\theta _m) } ={17.32 \over 10}}
$$

Asumiendo que la onda sinusoidal se encuentra en su fase positiva, y sustituyendo, se obtiene el siguiente ángulo (offset con respecto al cero del resolver):

$$
{\theta_m} = {\arctan(1.732)}= {1.047 \ rad \approx 60\deg}
$$

Se han contado 350 pasos por cero, es decir 350 revoluciones, lo que supone un giro de $350 \cdot 2 \pi = 2199.11\,rad$. Por tanto, el valor final del ángulo en el eje del motor es $\theta_{m1}=2200.16\,rad$.

La reducción del Harmonic Drive es de 175:1, de modo que este ángulo de giro en el eje del motor supone un giro de $\theta_{j}={2200.16 / 175}=12.57 \, rad$ en el eje de la articulación.

Para averiguar el desplazamiento lineal del piñón sobre la cremallera se tiene la ecuación que relaciona el ángulo de giro con la longitud de arco de circunferencia, dado su radio:

$$d = \theta \cdot r$$

Teniendo que el piñón tiene un radio efectivo de $0.3\,m$, la articulación lineal $q1$ en el punto de parada habrá avanzado esta distancia:

$$
d_1 = \theta_j \cdot r = 12.57 \cdot 0.3 = 3.77\,m
$$

#### Apartado b2

Teniendo la velocidad de giro del motor, la relación de transmisión permite obtener la velocidad de giro del piñón:

$$
N = {\omega_{m1} \over \omega_{1} } \quad \Longrightarrow \quad {\omega_{1}} = {\omega_{m1} \over N  } = {1100 \over 175} = 6.268\,rpm \equiv 0.658\,rad/s
$$

Transformando la velocidad angular del giro del piñón en velocidad lineal de la articulación $q1$:

$$
v_1 = \omega_1 \cdot r = 0.658 \cdot 0.3 = 0.197\,m/s
$$

## Problema 3

![](pictures/MorP3.svg)

Para el robot de 2 GDL ($q_1$ y $q_2$) de la figura anterior, diseñar los actuadores y sensores. Estos están compuestos, en cada eje, por un motor DC con encoder en su eje y un engranaje. Calcular:

1. Las reducciones que tienen que tener los engranajes, si se conoce que:
    * La velocidad máxima de los motores es de 6000&nbsp;rpm.
    * Al mover cada motor por separado (estando el otro parado), la velocidad máxima de la trayectoria en el extremo del robot es de 3000&nbsp;mm/s.
2. La resolución que tienen que tener los encoders de los ejes si se desea una resolución total de posicionamiento en el extremo de 0.01&nbsp;mm (los reductores son de juego cero).
3. El máximo peso que puede levantar el robot en su extremo en las condiciones más desfavorables, si se conoce que:
    * El par nominal (continuo) de cada motor es de 1&nbsp;Nm.
    * El coeficiente de aprovechamiento de los reductores es del 80&nbsp;%.
    * Los motores no tienen frenos.

### Solución

#### Apartado 1

Para ambas articulaciones, podemos relacionar la velocidad de giro del eje del motor $\omega_m$ con la velocidad de giro del eje de la articulación $\omega_j$ empleando relación de reducción:

$$
N = {\omega_m \over \omega_j}
$$

Por otro lado, la velocidad lineal en el extremo del robot se puede obtener mediante la expresión que la relaciona con la velocidad de giro de la articulación, $v = \omega_j \cdot r$, siendo $r$ el radio de giro (distancia entre el eje y el extremo).

Combinando ambas ecuaciones:

$$
N = {\omega_m \cdot r \over v}
$$

La velocidad lineal en el extremo del robot será máxima cuando el brazo esté totalmente extendido, es decir, cuando este radio de giro $r$ también sea máximo. Analizaremos por separado cada articulación para dimensionar sus correspondientes reductoras, teniendo en cuenta las unidades adecuadas (6000&nbsp;rpm equivalen a 628.32&nbsp;radianes) y la longitud combinada de los eslabones que intervienen en el movimiento.

**Articulación 1** ($r = l_1 + l_2$, con el eje $q_2$ bloqueado):

$$
N_1 = {\omega_{m1} \cdot (l_1 + l_2) \over v} = {628.32 \cdot (1.2 + 2) \over 3} = 670.21
$$

Para cumplir con los valores máximos de velocidad que especifica el enunciado, la relación de reducción debe ser mayor que 670.21. Aproximaremos a un número entero superior a este valor dado que no tiene sentido emplear fracciones. Siendo realistas, asumiremos que las reductoras disponibles en el mercado tendrían una relación de transmisión acotada a las centenas, de modo que tomaremos:

$$N_1 = 700$$

**Articulación 2** ($r = l_2$):

$$
N_2 = {\omega_{m2} \cdot l_2 \over v} = {628.32 \cdot 2 \over 3} = 418.88
$$

Repitiendo el razonamiento anterior:

$$N_2 = 500$$

#### Apartado 2

Se pide obtener la resolución total de posicionamiento en el extremo del robot. Dado que este tiene dos GDL, hallaremos la contribución de cada uno de ellos en la resolución lineal del extremo, y consideraremos la suma de ambas como la resolución total.

Primero, es necesario considerar la configuración del robot más desfavorable. Un giro mínimo (aquel que suponga un único pulso de encoder) en los ejes de los motores supondrá un desplazamiento máximo del extremo del brazo del robot cuando este se encuentre totalmente extendido, es decir, cuando $q_1 = q_2 = 0$.

En la figura siguiente se representan, de manera exagerada, unos giros infinitesimales $\theta_1$ y $\theta_2$ en cada articulación.

![](pictures/MorP3_1.svg)

Por trigonometría, podemos relacionar dichos giros con el desplazamiento lineal en el extremo, analizando la contribución de cada articulación por separado.

**Articulación 1** (radio de giro: $l_1 + l_2$, $q_2 = 0$):

Si relacionamos la resolución lineal $\Delta d$ con el giro $\theta_1$:

$$
sin(\theta_1 / 2) = {{\Delta d_1 / 2} \over {l_1 + l_2}} \quad \Longrightarrow \quad \Delta d_1 = 2 \cdot (l_1 + l_2) \cdot sin(\theta_1 / 2)
$$

**Articulación 2** (radio de giro: $l_2$):

$$
sin(\theta_2 / 2) = {{\Delta d_2 / 2} \over l_2} \quad \Longrightarrow \quad \Delta d_2 = 2 \cdot l_2 \cdot sin(\theta_2 / 2)
$$

Como simplificación, podemos asumir que $\theta_1 = \theta_2 = \theta$, entonces si la resolución total pedida es 0.01&nbsp;mm:

$$
\Delta d = \Delta d_1 + \Delta d_2 = 2 \cdot (l_1 + 2 \cdot l_2) \cdot sin(\theta / 2)  = 10^{-5}\,\text{[m]}\quad \Longrightarrow \quad \theta \approx 0.00011º
$$

Este es el ángulo máximo que puede girar cada articulación (por pulso del encoder) para cumplir con el requisito de resolución lineal en el extremo. Si lo expresamos en giro en el eje de cada motor:

$$
N_1 = {\theta_{m1} \over \theta_{j1}} = 700 \quad \Longrightarrow \quad \theta_{m1} = 0.077º
$$

$$
N_2 = {\theta_{m2} \over \theta_{j2}} = 500 \quad \Longrightarrow \quad \theta_{m2} = 0.055º
$$

La conversión a ranuras del encoder se obtendrá con la siguiente fórmula, suponiendo que cada ranura del encoder produce cuatro pulsos:

$$
{{1\,\text{ranura}} \over {4\,\text{pulsos}}} \cdot {{1\,\text{pulso}} \over {\theta_m\,\text{[º motor]}}} \cdot {360º \over {1\,\text{vuelta}}}
$$

Sustituyendo para cada motor, y aproximando al entero superior más próximo, se obtiene que el encoder del eje 1 debería tener un mínimo de 1167 ranuras, y el del eje 2, 1634 ranuras.

Si se pretendiese usar el mismo modelo de encoder en ambas articulaciones, ambos deberían tener al menos 1634 ranuras.

#### Apartado 3

Aplicaremos el balance de energía entre entrada (trabajo mecánico del motor) y salida (trabajo mecánico de la articulación) para determinar el par máximo que puede soportar el motor en el eje de la articulación, considerando un rendimiento $\eta = 0.8$.

$$
\eta = {E_s \over E_e} = {W_j \over W_m} = { {\tau_j \cdot \theta_j} \over { \tau_m \cdot \theta_m} } \quad \Longrightarrow \quad \tau_j = { {\tau_m \cdot \eta} \cdot N }
$$

Sustituyendo valores para obtener el par en la articulación $q_1$:

$$
\tau_1 = { {1 \cdot 0.80} \cdot 700 } = {560\,Nm}
$$

La configuración más desfavorable se muestra en la siguiente figura:

![](pictures/MorP3_2.svg)

La carga máxima que podrá levantar el robot con la articulación $q_2$ bloqueada y ejerciendo un par en la articulación $q_1$ será:

$$
\tau_1={ P \cdot (l_1 + l_2) } \quad \Longrightarrow \quad P = {\tau_1 \over {l_1 + l_2}}
$$

Sustituyendo:

$$
P = {560 \over {1.2 + 2}} = 175\,N \equiv 17.86\,kg
$$

Repitiendo el mismo razonamiento para la articulación $q_2$:

$$
\tau_2 = { {1 \cdot 0.80} \cdot 500 } = {400\,Nm}
$$

$$
P = {\tau_2 \over l_2} = {400 \over 2} = 200\,N \equiv 20.41\,kg
$$

Por consiguiente, la articulación $q_1$ es la más limitante. La carga máxima que puede levantar el robot en su extremo es de 17.86&nbsp;kg.

## Problema 4

![](pictures/MorP4.png)

El robot de la figura se mueve mediante un conjunto motor+encoder+reductor. El motor es de tipo AC, con un par máximo de 1&nbsp;Nm y una velocidad máxima de 10000&nbsp;rpm. El reductor es un Harmonic Drive con una relación de reducción de 150:1 y un rendimiento del 90&nbsp;%. El encoder tiene 100 cuentas por vuelta (líneas por revolución). La distancia entre el eje del motor y el extremo del robot ($l$) es de 1&nbsp;m. Se pide:

* a) Calcular el peso máximo $Q$ que puede soportar el robot en su extremo.
* b) Calcular la resolución lineal del movimiento en el extremo del robot.
* c) Si el motor gira a su velocidad máxima, ¿a qué velocidad angular (en grados por segundo) está girando la articulación?

### Solución

#### Apartado a

El par en la articulación para el caso más desfavorable será:

$$
\tau_j=Q \cdot l
$$

Aplicando un balance de energías en la articulación:

$$
\eta = {E_s \over E_e} = {W_q \over W_m} = { {\tau_j \cdot \theta_j} \over {\tau_m \cdot \theta_m} } \quad \Longrightarrow \quad \tau_j = {\eta \cdot \tau_m \cdot {\theta_m \over \theta_j}} = {\eta \cdot \tau_m \cdot N}
$$

Sustituyendo:

$$
\tau_j = 0.9 \cdot 1 \cdot 150 = 135\,Nm
$$

Entonces, la carga máxima será:

$$
Q = {\tau_j \over l} = {135 \over 1} = 135\,N \equiv 13.78\,kg
$$

#### Apartado b

Conociendo las características del encoder (número de ranuras), vamos a estimar la magnitud del giro en el eje del motor por cada pulso registrado, después el giro equivalente por pulso en el eje de la articulación, y finalmente traduciremos ese movimiento rotacional a un desplazamiento lineal del extremo del robot.

Vamos a asumir que cada ranura del encoder produce cuatro pulsos. Considerando que el encoder tiene 100 ranuras y que $N=150$:

$$
{100\,\text{ranuras} \over {1\,\text{vuelta}}} \cdot {4\,\text{pulsos} \over {1\,\text{ranura}}} \cdot {{1\,\text{vuelta}} \over {360\,\text{º motor}}} \cdot {{150\,\text{º motor}} \over {1\,\text{º articulación}}} = 166.67\,\text{pulsos/º articulación}
$$

Invirtiendo el resultado anterior, tenemos que un pulso del encóder supone un giro de 0.006º en el eje de la articulación.

Para convertir este giro en un desplazamiento lineal, recurrimos a la siguiente figura:

![](pictures/MorP4_1_ES.svg)

Por trigonometría, el lado $b$ es igual a:

$$
b = 2 \cdot L \cdot \sin \left( {\theta_j \over 2} \right)
$$

Teniendo que la hipotenusa $L$ es igual a la longitud del eslabon (1&nbsp;metro), y el giro en el eje de la articulación es $\theta_j = 0.006º$, la resolución lineal que se corresponde con esta distancia $b$ es:

$$res = 0.105\,mm$$

#### Apartado c

Empleando la expresión de la relación de transmisión de la reductora:

$$
N = {\omega_m \over \omega_j} \quad \Longrightarrow \quad \omega_j = {\omega_m \over N} = {10000 \over 150} = 66.67\,rpm \equiv 400\,º/s
$$
