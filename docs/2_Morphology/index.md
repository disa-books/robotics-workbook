

# Introduction

In this chapter, the morphology of mechanisms is studied. A mechanism is a system of bodies arranged to transmit motion in a predetermined way. A body is a single element of a mechanism. If this element is rigid, it is known as a _link_. Links are designed to define the shape and structure of the overall system. Two contiguous elements are defined as a kinematic pair if they have a permanent contact and a relative motion between them. This contact is guaranteed by _joints_, which constrain their relative motion. When several kinematic pairs are connected, they form a _kinematic chain_.

![](pictures/MorIntro.svg )

Joints and links are connected in a specific order to create the kinematic chain, which will define the degrees of freedom that the system will have. In a serial robot like the one depicted above, each joint connects two links. The first link is usually attached to a fixed base, while the last link holds the end-effector (e.g. a robotic gripper).

There are several robotic joint types. In this course, we are focused on planar mechanisms (two-dimensional), and more specifically on joints that produce rotational and linear relative motions between links.

