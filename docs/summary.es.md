# Table of contents

* [Introduction](index.md)
* [Fundamentals](1_Fundamentals/index.md)
    * [Basic concepts](1_Fundamentals/basic_concepts.md)
    * [Math tools](1_Fundamentals/math_tools.md)
* [Morphology](2_Morphology/index.md)
    * [Morphology](2_Morphology/Morphology.md)
* [Cinemática](3_kinematics/index.md)
    * [Denavit–Hartenberg](3_Kinematics/denavithartenberg.md)
    * [Cinemática Directa](3_Kinematics/cinematica_directa.md)
    * [Cinemática Inversa](3_Kinematics/cinematica_inversa.md)
* [Cinemática Diferencial](4_Differential_Kinematics/index.md)
    * [Cinemática D. Directa](4_Differential_Kinematics/cinematica_d_directa.md)
    * [Cinemática D. Inversa](4_Differential_Kinematics/cinematica_d_inversa.md)
* [Trayectorias](5_Trajectories/trayectorias.md)


