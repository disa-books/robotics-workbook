
---

This is the main page for the textbook of robotics subject

It is advised to use the book version:

  - [Book Online](https://disa-books.gitlab.io/robotics-workbook/)
  


But you can also navigate the markdown files in the code repository:

  - [Files](docs/summary.md)


Also the pdf file will be available soon.
